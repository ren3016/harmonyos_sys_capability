# HarmonyOS 系统能力检测 SystemCapability

#### 介绍
系统能力 SystemCapability 检测

#### 开发平台

Win11 + DevEco Stuido，本实例新建了自定义函数并引用

#### 开发步骤

1. 在 DevEco Studio 中 File->New_Create Project，选择 Application->Empty Ability， Next， Compile SDK：3.1.0(API 9)， Model: stage

2. 新建 ets\common\func.ets 并写入函数：

3. 修改 entry\src\main\resources\base\element\string.json 文件中的 EntryAbility_label 的 value 为 系统能力。 同时修改和 base 同级的 zh_CN、en_US 下的 string.json 文件中的 EntryAbility_label 的 value 为 系统能力

5. 使用 USB 链接手机 并打开手机 USB 调试

6. 在 File 中选择 Project Structure -> Signing Configs 选择 Support HarmonyOS 和 Automatically generate signature （需要打开浏览器登录华为帐号并授权）

7. 链接手机成功后， 点击右上角工具栏的真机调试。

8. 手机上自动添加了名为 系统能力 的应用

#### 主页代码

详见资源

#### string.json 配置文件修改
1. entry\src\main\resources\base\element\string.json  （默认配置文件）
2. entry\src\main\resources\zh_CN\element\string.json  （中文配置文件）
3. entry\src\main\resources\en_US\element\string.json  （英文配置文件）
```
{
  "string": [
    {
      "name": "module_desc",
      "value": "module description"
    },
    {
      "name": "EntryAbility_desc",
      "value": "系统能力 SystemCapability 检测"
    },
    {
      "name": "EntryAbility_label",
      "value": "系统能力"
    }
  ]
}
```

#### 相关权限

无

#### 克隆安装

git clone https://gitee.com/ren3016/harmonyos_http_request.git

#### 使用说明

打开应用，可以看到获取远程JSON数据后解析出来的值。

#### 约束与限制
1. 本示例仅支持标准系统上运行，支持设备：华为手机或运行在DevEco Studio上的华为手机设备模拟器。
2. 本示例为Stage模型，支持API Version 9。
3. 本示例需要使用DevEco Studio 3.1 Release版本进行编译运行。

#### 页面效果
![输入图片说明](entry/src/main/resources/rawfile/Sample_image.jpg)

